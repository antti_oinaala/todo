//
//  BiometricAuthenticationViewController.swift
//  ToDo
//
//  Created by Antti Oinaala on 20.5.2021.
//

import UIKit
import LocalAuthentication

final class BiometricAuthenticationViewController: UIViewController {
    private lazy var logInTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Biometric Authentication:"
        label.font = .systemFont(ofSize: 30)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(label)
        return label
    }()
    private lazy var logInButton: UIButton = {
       let button = UIButton()
        button.setTitle("Authenticate", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = Color.googleBlue
        button.contentEdgeInsets = UIEdgeInsets(top: 10, left: 40, bottom: 10, right: 40)
        button.addTarget(self, action: #selector(self.onAuthenticationAction(_:)), for: UIControl.Event.touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(button)
        return button
    }()
    
    var onBiometricAuthenticationSuccess: OnBiometricAuthenticationSuccess?
    /// An authentication context stored at class scope so it's available for use during UI updates.
    var authenticationContext = LAContext()
    /// The available states of being logged in or not.
    enum AuthenticationState {
        case loggedin, loggedout
    }

    /// The current authentication state.
    var state = AuthenticationState.loggedout
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // The biometryType, which affects this app's UI when state changes, is only meaningful
        //  after running canEvaluatePolicy. But make sure not to run this test from inside a
        //  policy evaluation callback (for example, don't put next line in the state's didSet
        //  method, which is triggered as a result of the state change made in the callback),
        //  because that might result in deadlock.
        authenticationContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
        
        // Set the initial app state. This impacts the initial state of the UI as well.
        state = .loggedout
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        logIn()
    }
    
    override func loadView() {
        super.loadView()
        
        initialize()
    }
}

// MARK: - Private functions

extension BiometricAuthenticationViewController {
    func initialize() {
        view.backgroundColor = .gray
        
        NSLayoutConstraint.activate([
            self.logInTitleLabel.bottomAnchor.constraint(equalTo: self.logInButton.topAnchor, constant: -5),
            self.logInTitleLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            
            self.logInButton.centerXAnchor.constraint(equalTo: self.logInTitleLabel.centerXAnchor),
            self.logInButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor),
        ])
    }
    
    func logIn() {
        if state == .loggedin {
            // Log out immediately.
            state = .loggedout

        } else {
            // Get a fresh context for each login. If you use the same context on multiple attempts
            //  (by commenting out the next line), then a previously successful authentication
            //  causes the next policy evaluation to succeed without testing biometry again.
            //  That's usually not what you want.
            authenticationContext = LAContext()
            authenticationContext.localizedFallbackTitle = "Please use your Passcode"
          //  authenticationContext.localizedCancelTitle = "Cancel"

            // First check if we have the needed hardware support.
            var error: NSError?
            let reason = "Authentication required to access the secured data"
            if authenticationContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
                
                authenticationContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason ) { success, error in
                    if success {
                        // Move to the main thread because a state update triggers UI changes.
                        DispatchQueue.main.async { [unowned self] in
                            self.state = .loggedin
                            
                            self.dismiss(animated: false) {
                                self.onBiometricAuthenticationSuccess?()
                            }
                        }
                    } else {
                        print(error?.localizedDescription ?? "Failed to authenticate")
                    }
                }
            } else {
                print(error?.localizedDescription ?? "Can't evaluate policy")
            }
        }
    }
}

// MARK: - Button callbacks

extension BiometricAuthenticationViewController {
    @objc func onAuthenticationAction(_ sender: UIButton) {
        logIn()
    }
}
