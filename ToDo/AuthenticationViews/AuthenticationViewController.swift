//
//  AuthenticationViewController.swift
//  CarRental
//
//  Created by Antti Oinaala on 11.3.2021.
//

import UIKit
import GoogleSignIn

final class AuthenticationViewController: UIViewController {
    lazy var saveTaskButton: GoogleButton = {
        let button = GoogleButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(onAuthenticationAction(_:)), for: UIControl.Event.touchUpInside)
        self.view.addSubview(button)
        return button
    }()
    let googleSignIn = GIDSignIn.sharedInstance()
    var onAuthenticationCompleted: OnAuthenticationCompleted?
    
    override func loadView() {
        super.loadView()
        
        self.initialize()
    }
    
    func tryToSignInSilently() {
        googleSignIn?.clientID = googleUserId
        googleSignIn?.delegate = self
        googleSignIn?.restorePreviousSignIn()
    }
}


// MARK: - Private functions

private extension AuthenticationViewController {
    func initialize() {
        self.view.backgroundColor = .lightGray
        
        NSLayoutConstraint.activate([
            self.saveTaskButton.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor),
            self.saveTaskButton.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor),
            self.saveTaskButton.leftAnchor.constraint(greaterThanOrEqualTo: view.safeAreaLayoutGuide.leftAnchor, constant: 5),
            self.saveTaskButton.rightAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.rightAnchor, constant: -5),
            self.saveTaskButton.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    func googleAuthLogin() {
        googleSignIn?.presentingViewController = self
        googleSignIn?.clientID = googleUserId
        googleSignIn?.delegate = self
        googleSignIn?.signIn()
    }
}

// MARK: - Google sign in delegate

extension AuthenticationViewController: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if (error == nil) {
            // Perform any operations on signed in user here.
            // ...
        } else {
            print("\(error.localizedDescription)")
        }
        
        guard let user = user else { return }
        
        dismiss(animated: true) {
            self.onAuthenticationCompleted?(user.profile.givenName)
        }
    }
}

// MARK: - Button callbacks

extension AuthenticationViewController {
    @objc func onAuthenticationAction(_ sender: UIButton) {
        googleAuthLogin()
    }
}
