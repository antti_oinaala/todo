//
//  DatePickerTextField.swift
//  ToDo
//
//  Created by Antti Oinaala on 3.5.2021.
//

import Foundation
import UIKit

final class DatePickerTextField: UITextField, UITextFieldDelegate {
    private let datePicker = UIDatePicker(frame: .zero)
    
    var dateSelected: DatePickerTextFieldDateSelected?
    var longDateString: String? {
        didSet {
            // Setup date picker roll
            guard let date = Date.dateTimeFromString(str: longDateString) else { return }
            datePicker.date = date
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.initialize()
    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return .zero
    }
    
    // Delegate prevents the user editing text on textfield
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return false
    }
}

// MARK: - Private functions

private extension DatePickerTextField {
    func initialize() {
        self.inputView = datePicker
        self.datePicker.addTarget(self, action: #selector(dateValueChanged), for: .valueChanged)
        self.datePicker.preferredDatePickerStyle = .wheels
        self.datePicker.datePickerMode = .date
        self.datePicker.sizeToFit()
        
        self.delegate = self
        
        guard let keyWindowWidth = UIApplication.shared.keyWindow?.frame.width else { return }
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: keyWindowWidth, height: 44.0))
        toolbar.barStyle = .default
 
        let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(onCancelAction))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(onDoneAction))
        toolbar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        self.inputAccessoryView = toolbar
    }
    
    func updateText() {
        let date = datePicker.date
        longDateString = Date.dateStringFromDate(datetime: date)
        guard let shortDateString = Date.shortDateStringFromString(str: longDateString) else { return }
        
        self.text = shortDateString
    }
}

// MARK: - Actions

@objc extension DatePickerTextField {
    private func dateValueChanged() {
        updateText()
    }
    
    private func onCancelAction() {
        resignFirstResponder()
    }
    
    private func onDoneAction() {
        updateText()
        
        dateSelected?(longDateString)
        
        resignFirstResponder()
    }
}
