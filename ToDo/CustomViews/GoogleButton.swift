//
//  GoogleButton.swift
//  ToDo
//
//  Created by Antti Oinaala on 18.5.2021.
//

import UIKit

final class GoogleButton: UIButton {
    private var didLayoutSubviews = false
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private var shadowLayer: CAShapeLayer?

    override func layoutSubviews() {
        defer {
            didLayoutSubviews = true
            super.layoutSubviews()
        }
        
        guard didLayoutSubviews == false else { return }

        let shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: bounds, cornerRadius: 3).cgPath
        if self.backgroundColor != nil {
            shadowLayer.fillColor = self.backgroundColor?.cgColor
        }
        else{
            shadowLayer.fillColor = UIColor.white.cgColor
        }
        shadowLayer.shadowColor = UIColor.black.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 3.0)
        shadowLayer.shadowOpacity = 0.4
        shadowLayer.shadowRadius = 3
        
        layer.insertSublayer(shadowLayer, at: 0)
    }
}

// MARK: - Private functions

private extension GoogleButton {
    func initialize() {
        self.setTitle("Sign in with Google", for: .normal)
        self.titleLabel?.font = UIFont(name: "Roboto-Medium", size: 16)
        self.setImage(UIImage(named: "GoogleIcon"), for: .normal)
        self.tintColor = .white
        self.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 40)
        self.imageEdgeInsets = UIEdgeInsets(top: -2, left: -2, bottom: -2, right: -21)
        self.titleEdgeInsets = UIEdgeInsets(top: 0, left: 6, bottom: 0, right: -15)
        self.contentVerticalAlignment = .fill
        self.contentHorizontalAlignment = .fill
        
        self.setTitleColor(.white, for: .normal)
        self.backgroundColor = Color.googleBlue
        self.layer.cornerRadius = 3.0
    }
}
