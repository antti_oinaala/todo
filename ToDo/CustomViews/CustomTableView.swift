//
//  CustomTableView.swift
//  ToDo
//
//  Created by Antti Oinaala on 26.3.2021.
//

import UIKit

final class CustomTableView: UITableView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if !(__CGSizeEqualToSize(bounds.size,self.intrinsicContentSize)){
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return contentSize
    }
}
