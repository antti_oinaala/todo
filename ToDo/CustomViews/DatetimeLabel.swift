//
//  DatetimeLable.swift
//  ToDo
//
//  Created by Antti Oinaala on 13.5.2021.
//

import UIKit

final class DatetimeLabel: UIView {
    lazy var label: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(Font.sizeSmall)
        label.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(label)
        return label
    }()
    
    var dateString: String? {
        didSet {
            let expirationDate = Date.dateTimeFromString(str: dateString)
            guard let days = Calendar.dayCountFromDates(start: Date(), end: expirationDate) else { return }
            
            if days == 1 {
                backgroundColor = .yellow
                label.textColor = .gray
            } else if days <= 0 {
                backgroundColor = .red
                label.textColor = .white
            } else {
                backgroundColor = .clear
                label.textColor = .white
            }
            
            label.text = Date.shortDateStringFromString(str: dateString)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension DatetimeLabel {
    func initialize() {
        self.layer.cornerRadius = 3.0
        self.clipsToBounds = true
        
        NSLayoutConstraint.activate([
            self.label.topAnchor.constraint(equalTo: self.topAnchor, constant: 1),
            self.label.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 3),
            self.label.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -3),
            self.label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -1),
        ])
    }
}
