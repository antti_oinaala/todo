//
//  LogButton.swift
//  ToDo
//
//  Created by Antti Oinaala on 18.5.2021.
//

import UIKit
import GoogleSignIn

final class LogButton: UIView {
    private lazy var letterAvatarLabel: LetterAvatarLabel = {
        let label = LetterAvatarLabel()
        label.layer.cornerRadius = LetterAvatar.sizeMid / 2.0
        label.clipsToBounds = true
        label.font = label.font.withSize(Font.sizeBig)
        label.name = GIDSignIn.sharedInstance()?.currentUser?.profile.name
        label.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(label)
        return label
    }()
    private lazy var titleLabel: UILabel = {
        let label = UILabel()
        if GIDSignIn.sharedInstance()?.currentUser?.profile.name == nil {
            label.text = "Sign In"
        } else {
            label.text = "Sign Out"
        }
        label.font = label.font.withSize(16)
        label.textColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(label)
        return label
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Private functions

private extension LogButton {
    func initialize() {
        NSLayoutConstraint.activate([
            self.letterAvatarLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            self.letterAvatarLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 5),
            self.letterAvatarLabel.widthAnchor.constraint(equalToConstant: LetterAvatar.sizeMid),
            self.letterAvatarLabel.heightAnchor.constraint(equalToConstant: LetterAvatar.sizeMid),
            self.letterAvatarLabel.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor),
            self.letterAvatarLabel.bottomAnchor.constraint(lessThanOrEqualTo: self.bottomAnchor),
            
            self.titleLabel.centerYAnchor.constraint(equalTo: self.letterAvatarLabel.centerYAnchor),
            self.titleLabel.leftAnchor.constraint(equalTo: self.letterAvatarLabel.rightAnchor, constant: 5),
            self.titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: -5),
        ])
    }
}
