//
//  Constants.swift
//  ToDo
//
//  Created by Antti Oinaala on 22.4.2021.
//

import Foundation
import UIKit

let googleUserId = "659919299352-lnk3cshinvv0hasvoqricqeu9bgrhjv2.apps.googleusercontent.com"

// Tuples
typealias SetupViewDataCollection = (tableName: String, task: Table.Task)

enum TableNames: String {
    case toDo = "To Do"
    case done = "Done"
    
    static let allValues = [toDo, done]
}

enum TaskAction: Int {
    case edit
    case delete
    case changeTable
}

struct URL {
    static let baseURLPath = "todotasksapinodejs.herokuapp.com"
}

struct LetterAvatar {
    static let sizeMid: CGFloat = 30.0
    static let sizeBig: CGFloat = 37.0
}

struct Icon {
    static let sizeSmall: CGFloat = 20.0
    static let sizeMid: CGFloat = 25.0
    static let sizeBig: CGFloat = 40.0
}

struct Font {
    static let sizeSmall: CGFloat = 12
    static let sizeMid: CGFloat = 14
    static let sizeBig: CGFloat = 16
    static let sizeTitle: CGFloat = 20
}

struct Setup {
    static let cellNumber = 4
    static let tableViewRowHeight: CGFloat = 60.0
}

struct Color {
    static let googleBlue = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
    static let contentBackground = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 0.6616175477)
}
