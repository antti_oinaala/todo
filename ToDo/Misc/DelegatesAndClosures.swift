//
//  Delegates.swift
//  ToDo
//
//  Created by Antti Oinaala on 24.4.2021.
//

import Foundation

typealias DatePickerTextFieldDateSelected = ((String?) -> Void)
typealias OnUpdatingTaskTable = ((_ task: Table.Task?,_ tableName: String?, _ action: TaskAction) -> Void)
typealias OnBiometricAuthenticationSuccess = (() -> Void)
typealias OnAuthenticationCompleted = ((String) -> Void)
typealias OnTaskOwnerSelected = ((String?) -> Void)
typealias OnDateTimeSelected = ((String?) -> Void)
typealias OnTaskStateSelected = ((String?) -> Void)

protocol TaskDelegate: class {
    func onCreatingNewTaskOnTable(tableName: String?)
    func onCancelingCreatingNewTask()
    func onEditing(task: Table.Task?, OnTable tableName: String?, taskAction: TaskAction)
    func didSaveTaskOnTable()
}
