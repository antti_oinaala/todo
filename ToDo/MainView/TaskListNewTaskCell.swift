//
//  TaskListAddCell.swift
//  ToDo
//
//  Created by Antti Oinaala on 23.4.2021.
//

import UIKit
import Gemini

final class TaskListNewTaskCell: GeminiCell {
    private lazy var bgView: UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.layer.cornerRadius = 5.0
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(view)
        return view
    }()
    private lazy var tableView: CustomTableView = {
        let tableView = CustomTableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        dataSource.reloadTableView = tableView.reloadData
        tableView.translatesAutoresizingMaskIntoConstraints = false
        bgView.addSubview(tableView)
        tableView.register(TaskCell.self, forCellReuseIdentifier: String(describing: TaskCell.self))
        return tableView
    }()
    private lazy var taskListTitleView: TaskListTitleView = {
        let view = TaskListTitleView()
        view.backgroundColor = .lightGray
        view.translatesAutoresizingMaskIntoConstraints = false
        bgView.addSubview(view)
        return view
    }()
    
    lazy var taskListEditListView: TaskListEditListView = {
        let view = TaskListEditListView()
        self.backgroundColor = .black
        view.translatesAutoresizingMaskIntoConstraints = false
        bgView.addSubview(view)
        return view
    }()
    private let dataSource = TaskListDataSource()
    var table: Table? = nil {
        didSet {
            guard let tableItem = table else { return }
            
            taskListTitleView.titleLabel.text = tableItem.name
            dataSource.setTable(table: tableItem)
            dataSource.onUpdatingTaskTable = onUpdatingTaskTable
        }
    }
    
    var onUpdatingTaskTable: OnUpdatingTaskTable?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
}

extension TaskListNewTaskCell {
    func initialize() {
        tableView.delegate = dataSource
        tableView.dataSource = dataSource
       
        NSLayoutConstraint.activate([
            self.bgView.topAnchor.constraint(equalTo: taskListTitleView.topAnchor),
            self.bgView.leftAnchor.constraint(equalTo: taskListTitleView.leftAnchor),
            self.bgView.rightAnchor.constraint(equalTo: taskListTitleView.rightAnchor),
            self.bgView.bottomAnchor.constraint(equalTo: taskListEditListView.bottomAnchor),
            
            self.taskListTitleView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            self.taskListTitleView.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor),
            self.taskListTitleView.rightAnchor.constraint(equalTo: safeAreaLayoutGuide.rightAnchor),
            self.taskListTitleView.heightAnchor.constraint(equalToConstant: 34),
            
            self.tableView.topAnchor.constraint(equalTo: taskListTitleView.bottomAnchor),
            self.tableView.leftAnchor.constraint(equalTo: taskListTitleView.leftAnchor),
            self.tableView.rightAnchor.constraint(equalTo: taskListTitleView.rightAnchor),
            
            self.taskListEditListView.topAnchor.constraint(equalTo: tableView.bottomAnchor),
            self.taskListEditListView.leftAnchor.constraint(equalTo: tableView.leftAnchor),
            self.taskListEditListView.rightAnchor.constraint(equalTo: tableView.rightAnchor),
            self.taskListEditListView.bottomAnchor.constraint(lessThanOrEqualTo: safeAreaLayoutGuide.bottomAnchor),
        ])
    }
}
