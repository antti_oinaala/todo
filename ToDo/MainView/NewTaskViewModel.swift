//
//  NewTaskViewModel.swift
//  ToDo
//
//  Created by Antti Oinaala on 23.4.2021.
//

import UIKit

final class NewTaskViewModel: NSObject {
    private var taskContentString: String?
    private var table: Table?
    weak var newTaskDelegate: TaskDelegate?
    
    convenience init(table: Table?) {
        self.init()
        
        self.table = table
    }
    
    func representationAsCellOnCollectionView(collectionView: UICollectionView, indexPath: IndexPath, newTask: Bool = false) -> UICollectionViewCell {
        if newTask {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TaskListNewTaskCell.self), for: indexPath) as? TaskListNewTaskCell else { return UICollectionViewCell() }
            
            setupTaskListNewTaskCell(cell: cell)
            
            return cell
        } else {
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing: TaskListCell.self), for: indexPath) as? TaskListCell else { return UICollectionViewCell() }
            
            setupTaskListCell(cell: cell)
            
            return cell
        }
    }
}

private extension NewTaskViewModel {
    func setupTaskListCell(cell: TaskListCell) {
        cell.taskListAddTaskView.addCardButton.addTarget(self, action: #selector(onCreatingNewTask(_:)), for: UIControl.Event.touchUpInside)
        
        cell.onUpdatingTaskTable = { [weak self] task, tableName, action in
            self?.newTaskDelegate?.onEditing(task: task, OnTable: tableName, taskAction: action)
        }
        
        cell.contentView.backgroundColor = .white
        
        cell.table = table
    }
    
    func setupTaskListNewTaskCell(cell: TaskListNewTaskCell) {
        cell.taskListEditListView.taskTextField.addTarget(self, action: #selector(onTextFieldDidChanged(sender:)), for: .editingChanged)
        
        cell.taskListEditListView.saveTaskButton.addTarget(self, action: #selector(onSavingNewTask(_:)), for: UIControl.Event.touchUpInside)
        
        cell.taskListEditListView.cancelButton.addTarget(self, action: #selector(onCancellingToCreateNewTask(_:)), for: UIControl.Event.touchUpInside)
        
        cell.onUpdatingTaskTable = { [weak self] task, tableName, action in
            self?.newTaskDelegate?.onEditing(task: task, OnTable: tableName, taskAction: action)
        }
        
        cell.taskListEditListView.taskTextField.text = nil
        
        cell.contentView.backgroundColor = .white
        
        cell.table = table
    }
}

@objc extension NewTaskViewModel {
    func onTextFieldDidChanged(sender: UITextField) {
        taskContentString = sender.text
    }
    
    func onCreatingNewTask(_ sender: UIButton) {
        newTaskDelegate?.onCreatingNewTaskOnTable(tableName: table?.name)
    }
    
    func onSavingNewTask(_ sender: UIButton) {
        guard var table = self.table, let text = taskContentString else { return }
        let task = Table.Task(id: UUID().uuidString, body: text, expirationDate: nil, owner: nil)

        table.appendTask(task: task)
        
        ConnectionManager.shared.updateData(data: table).done({ [weak self] updatedTable in
            self?.newTaskDelegate?.didSaveTaskOnTable()
        }).catch { error in
            print(error.localizedDescription)
        }
    }
    
    func onCancellingToCreateNewTask(_ sender: UIButton) {
        newTaskDelegate?.onCancelingCreatingNewTask()
    }
}
