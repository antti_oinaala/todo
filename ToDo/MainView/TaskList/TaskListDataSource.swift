//
//  TaskListDataSource.swift
//  ToDo
//
//  Created by Antti Oinaala on 21.4.2021.
//

import UIKit
import GoogleSignIn

final class TaskListDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    var reloadTableView: () -> Void = {}
    var onUpdatingTaskTable: OnUpdatingTaskTable?
    
    private var models = [TaskViewModel]()
    private var table: Table? {
        didSet {
            resetModels()
            
            reloadTableView()
        }
    }
    
    private var onCreatingNewTask = false
    
    func setTable(table: Table) {
        self.table = table
    }
    
    func reset() {
        resetModels()
    }
}

// MARK: - Table view delegates

extension TaskListDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = models[indexPath.row]
        
        return model.representationAsCellOnTableView(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let tableName = table?.name else { return }
        
        guard let authenticatedUserName = GIDSignIn.sharedInstance()?.currentUser?.profile.name else { return }
        
        if authenticatedUserName == models[indexPath.row].getTask().owner
            || models[indexPath.row].getTask().owner == nil {
            let vc = SetupViewController()
            
            vc.onUpdatingTaskTable = onUpdatingTaskTable
          
            vc.data = (tableName: tableName, task: models[indexPath.row].getTask())
            vc.modalPresentationStyle = .fullScreen
            UIApplication.shared.topMostViewController()?.present(vc, animated: true, completion: nil)
        }
    }
}

// MARK: - Private

private extension TaskListDataSource {
    func resetModels() {
        models.removeAll()
        
        table?.tasks.forEach { [weak self] task in
            let model = TaskViewModel(item: task)
            self?.models.append(model)
        }
    }
    
    func updateTask(task: Table.Task) {
        guard var table = self.table else { return }
        let i = table.tasks.firstIndex { $0.id == task.id }
        guard let index = i else { return }
        table.tasks[index] = task
        
        ConnectionManager.shared.updateData(data: table).done { [weak self] updatedTable in
            self?.table = updatedTable
            self?.resetModels()
            self?.reloadTableView()
        }.catch { error in
            print(error.localizedDescription)
        }
    }
}
