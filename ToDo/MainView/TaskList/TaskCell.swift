//
//  TaskCell.swift
//  ToDo
//
//  Created by Antti Oinaala on 21.4.2021.
//

import UIKit

final class TaskCell: UITableViewCell {
    lazy var contentBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Color.contentBackground
        view.layer.cornerRadius = 3.0
        view.clipsToBounds = true
        self.contentView.addSubview(view)
        return view
    }()
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = titleLabel.font.withSize(Font.sizeBig)
        titleLabel.textColor = .white
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(titleLabel)
        return titleLabel
    }()
    
    lazy var datetimeLabel: DatetimeLabel = {
        let label = DatetimeLabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(label)
        return label
    }()
    
    lazy var letterAvatarLabel: LetterAvatarLabel = {
        let label = LetterAvatarLabel()
        label.layer.cornerRadius = LetterAvatar.sizeBig / 2.0
        label.clipsToBounds = true
        label.font = label.font.withSize(Font.sizeTitle)
        label.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(label)
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.initialize()
        
        setNeedsUpdateConstraints()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.initialize()
    }
}

private extension TaskCell {
    func initialize() {
        NSLayoutConstraint.activate([
            self.contentBackgroundView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            self.contentBackgroundView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: 10),
            self.contentBackgroundView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -10),
            self.contentBackgroundView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            self.titleLabel.topAnchor.constraint(equalTo: contentBackgroundView.topAnchor, constant: 5),
            self.titleLabel.leftAnchor.constraint(equalTo: contentBackgroundView.leftAnchor, constant: 5),
            self.titleLabel.rightAnchor.constraint(equalTo: contentBackgroundView.rightAnchor, constant: -5),
            
            self.datetimeLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            self.datetimeLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            self.datetimeLabel.rightAnchor.constraint(greaterThanOrEqualTo: titleLabel.leftAnchor, constant: 5),
            
            self.letterAvatarLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            self.letterAvatarLabel.bottomAnchor.constraint(equalTo: contentBackgroundView.bottomAnchor, constant: -5),
            self.letterAvatarLabel.rightAnchor.constraint(equalTo: contentBackgroundView.rightAnchor, constant: -5),
            self.letterAvatarLabel.widthAnchor.constraint(equalToConstant: LetterAvatar.sizeBig),
            self.letterAvatarLabel.heightAnchor.constraint(equalToConstant: LetterAvatar.sizeBig),
        ])
    }
}
