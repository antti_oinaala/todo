//
//  File.swift
//  ToDo
//
//  Created by Antti Oinaala on 23.4.2021.
//

import UIKit

final class TaskListAddTaskView: UIView {
    lazy var addCardButton: UIButton = {
        let button = UIButton()
        button.setTitle("Add Card", for: .normal)
        button.tintColor = .white
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .black
        button.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(button)
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension TaskListAddTaskView {
    func initialize() {
        NSLayoutConstraint.activate([
            addCardButton.topAnchor.constraint(equalTo: topAnchor),
            addCardButton.bottomAnchor.constraint(equalTo: bottomAnchor),
            addCardButton.leftAnchor.constraint(equalTo: leftAnchor),
            addCardButton.rightAnchor.constraint(equalTo: rightAnchor),
        ])
    }
}
