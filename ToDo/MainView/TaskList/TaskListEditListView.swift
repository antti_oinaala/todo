//
//  TaskListNewTaskView.swift
//  ToDo
//
//  Created by Antti Oinaala on 23.4.2021.
//

import UIKit

final class TaskListEditListView: UIView {
    lazy var taskTextField: UITextField = {
        let textField = UITextField()
        textField.backgroundColor = .white
        textField.textColor = .black
        textField.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(textField)
        return textField
    }()
    
    lazy var cancelButton: UIButton = {
        let button = UIButton()
        button.setTitle("Cancel", for: .normal)
        button.tintColor = .white
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .darkGray
        button.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(button)
        return button
    }()
    
    lazy var saveTaskButton: UIButton = {
        let button = UIButton()
        button.setTitle("Save", for: .normal)
        button.tintColor = .white
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .darkGray
        button.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(button)
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension TaskListEditListView {
    func initialize() {
        NSLayoutConstraint.activate([
            taskTextField.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            taskTextField.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
            taskTextField.rightAnchor.constraint(equalTo: rightAnchor, constant: -5),
            
            cancelButton.topAnchor.constraint(equalTo: taskTextField.bottomAnchor, constant: 5),
            cancelButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -5),
            cancelButton.leftAnchor.constraint(equalTo: taskTextField.leftAnchor),
            cancelButton.widthAnchor.constraint(equalToConstant: 80),
            
            saveTaskButton.topAnchor.constraint(equalTo: cancelButton.topAnchor),
            saveTaskButton.bottomAnchor.constraint(equalTo: cancelButton.bottomAnchor),
            saveTaskButton.widthAnchor.constraint(equalToConstant: 80),
            saveTaskButton.rightAnchor.constraint(equalTo: rightAnchor, constant: -5),
        ])
    }
}
