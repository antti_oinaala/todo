//
//  TaskViewModel.swift
//  ToDo
//
//  Created by Antti Oinaala on 21.4.2021.
//

import UIKit

struct TaskViewModel {
    private let task: Table.Task
    
    init(item: Table.Task) {
        self.task = item
    }
    
    func representationAsCellOnTableView(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TaskCell.self), for: indexPath) as? TaskCell else { return UITableViewCell() }
        
        setupCell(cell: cell)
        
        return cell
    }
    
    func getTask() -> Table.Task {
        return task
    }
}

private extension TaskViewModel {
    func setupCell(cell: TaskCell) {
        cell.selectionStyle = .none
        cell.contentView.backgroundColor = .black
        
        cell.titleLabel.text = "\(task.body)"
        cell.letterAvatarLabel.name = task.owner
        cell.datetimeLabel.dateString = task.expirationDate
    }
}
