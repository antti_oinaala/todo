//
//  DataSource.swift
//  ToDo
//
//  Created by Antti Oinaala on 20.4.2021.
//

import Foundation
import Gemini
import PromiseKit

final class DataSource: NSObject {
    private var tables: [Table]?
    
    var reloadCollectionView: () -> Void = {}
    
    private var tableNameOfNewTask: String?
    
    private var models: [NewTaskViewModel]?
    
    func fetchData() -> Promise<Bool> {
        Promise { seal in
            tables = [Table]()
            
            ConnectionManager.shared.fetchData().done { [weak self] tables in
                if tables.count == 0 {
                    // There is no data structure yet
                    seal.fulfill(false)
                }
                guard let sortedTables = self?.sortTables(tables: tables) else { return }
                self?.tables = sortedTables
                self?.resetModels()
                self?.reloadCollectionView()
                seal.fulfill(true)
            }.catch { error in
                seal.reject(error)
            }
        }
    }
    
    func clearData() {
        tables = nil
        resetModels()
        reloadCollectionView()
    }
    
    func createDataStructure() {
        var tables = [Table]()
        TableNames.allValues.forEach {
            tables.append(Table(id: UUID().uuidString, name: $0.rawValue, tasks: []))
        }
        
        let promises: [Promise<Table>] = tables.compactMap { return ConnectionManager.shared.saveData(data: $0) }
        
        firstly {
            when(resolved: promises)
        }.done { [weak self] _ in
            self?.fetchData().done({_ in }).catch({_ in })
        }
    }
}

// MARK: - Collection view delegate functions

extension DataSource: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let model = models?[indexPath.row] else { return UICollectionViewCell() }
        
        if tableNameOfNewTask == TableNames.allValues[indexPath.row].rawValue {
            return model.representationAsCellOnCollectionView(collectionView: collectionView, indexPath: indexPath, newTask: true)
        } else {
            return model.representationAsCellOnCollectionView(collectionView: collectionView, indexPath: indexPath)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
}

//MARK: - Delegates

extension DataSource: TaskDelegate {
    func onEditing(task: Table.Task?, OnTable tableName: String?, taskAction: TaskAction) {
        weak var _self = self
        
        var newTables: [Table]?
        
        switch taskAction {
        case .delete:
            newTables = _self?.deleteTask(task: task)
        case .edit:
            newTables = _self?.updateTask(task: task, fromTable: tableName)
        case .changeTable:
            newTables = _self?.changeTasksTable(task: task, tableName: tableName)
        }

        _self?.tables = newTables
        _self?.resetModels()
        _self?.reloadCollectionView()
        _self?.saveDataChanges(newTables: newTables)
    }
    
    func onCreatingNewTaskOnTable(tableName: String?) {
        tableNameOfNewTask = tableName
        
        reloadCollectionView()
    }
    
    func onCancelingCreatingNewTask() {
        tableNameOfNewTask = ""
        
        reloadCollectionView()
    }
    
    func didSaveTaskOnTable() {
        fetchData()
            .done({_ in })
            .catch({ error in print(error.localizedDescription) })
    }
}

// MARK: - Private functions

private extension DataSource {
    func changeTasksTable(task: Table.Task?, tableName: String?) -> [Table] {
        weak var _self = self
        var newTables = [Table]()
        // Search and remove task from old table
        _self?.tables?.forEach { table in
            if let index = table.tasks.firstIndex(where: { $0.id == task?.id }) {
                var tableCopy = table
                tableCopy.tasks.remove(at: index)
                newTables.append(tableCopy)
                return
            }
        }
        
        // Add to new table
        _self?.tables?.forEach { table in
            if table.name == tableName {
                guard let task = task else { return }
                var tableCopy = table
                tableCopy.tasks.append(task)
                newTables.append(tableCopy)
                return
            }
        }
        
        return newTables
    }
    
    func updateTask(task: Table.Task?, fromTable named: String?) -> [Table] {
        weak var _self = self
        var newTables = [Table]()
        
        // Add to new table
        _self?.tables?.forEach { table in
            if table.name == named {
                let i = table.tasks.firstIndex { $0.id == task?.id }
                guard let index = i, let task = task else { return }
                var tableCopy = table
                tableCopy.tasks[index] = task
                newTables.append(tableCopy)
            } else {
                newTables.append(table)
            }
        }
        
        return newTables
    }
    
    func deleteTask(task: Table.Task?) -> [Table] {
        weak var _self = self
        var newTables = [Table]()
        // Search and remove task from old table
        _self?.tables?.forEach { table in
            if let index = table.tasks.firstIndex(where: { $0.id == task?.id }) {
                var tableCopy = table
                tableCopy.tasks.remove(at: index)
                newTables.append(tableCopy)
            } else {
                newTables.append(table)
            }
        }
        
        return newTables
    }
    
    func saveDataChanges(newTables: [Table]?) {
        guard let newTables = newTables else { return }
        let updatePromises: [Promise<Table>] = newTables.compactMap { return ConnectionManager.shared.updateData(data: $0) }
        
        firstly {
            when(resolved: updatePromises)
        }.done { [weak self] _ in
            self?.fetchData()
                .done({_ in })
                .catch({ error in print(error.localizedDescription) })
        }
    }
    
    func sortTables(tables: [Table]) -> [Table] {
        return TableNames.allValues.compactMap { tableName in tables.filter { tableName.rawValue == $0.name }.first }
    }
    
    func resetModels() {
        models = [NewTaskViewModel]()
        
        tables?.forEach { [weak self] table in
            let model = NewTaskViewModel(table: table)
            model.newTaskDelegate = self
            self?.models?.append(model)
        }
    }
}
