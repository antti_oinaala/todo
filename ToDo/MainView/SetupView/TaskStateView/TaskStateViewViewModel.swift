//
//  TaskStateViewViewModel.swift
//  ToDo
//
//  Created by Antti Oinaala on 10.5.2021.
//

import UIKit
import GoogleSignIn

final class TaskStateViewViewModel: NSObject {
    private var tableName: String?
    var currentTableName: String?
    
    convenience init(tableName: String?, currentTableName: String?) {
        self.init()
        
        self.tableName = tableName
        self.currentTableName = currentTableName
    }
    
    func representationAsCellOnTableView(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: OwnerCell.self), for: indexPath) as? OwnerCell else { return UITableViewCell() }
        
        setupOwnerCell(cell: cell)
        
        return cell
    }
}

private extension TaskStateViewViewModel {
    func setupOwnerCell(cell: OwnerCell) {
        cell.selectionStyle = .none
        cell.backgroundColor = .black
        
        guard let tableName = tableName, let currentTableName = currentTableName else { return }
        
        cell.ownerLabel.text = tableName
        
        cell.selectedImageView.isHidden = (currentTableName == tableName) ? false : true
    }
}
