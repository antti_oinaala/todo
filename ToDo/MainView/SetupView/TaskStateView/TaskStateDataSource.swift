//
//  TaskStateDataSource.swift
//  ToDo
//
//  Created by Antti Oinaala on 10.5.2021.
//

import UIKit
import FontAwesome_swift

final class TaskStateDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    private var models = [TaskStateViewViewModel]()
    var onTaskStateSelected: OnTaskStateSelected?
    var currentTableName: String? {
        didSet {
            resetModels(currentTableName: currentTableName)
        }
    }
}

// MARK: - Table view delegates

extension TaskStateDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = models[indexPath.row]
        let cell = model.representationAsCellOnTableView(tableView: tableView, indexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        onTaskStateSelected?(TableNames.allValues[indexPath.row].rawValue)
        resetModels(currentTableName: TableNames.allValues[indexPath.row].rawValue)
        tableView.reloadData()
    }
}

// MARK: - Private functions

extension TaskStateDataSource {
    func resetModels(currentTableName: String?) {
        models.removeAll()
        
        TableNames.allValues.forEach {
            let model = TaskStateViewViewModel(tableName: $0.rawValue, currentTableName: currentTableName)
            models.append(model)
        }
    }
}
