//
//  OwnerViewController.swift
//  ToDo
//
//  Created by Antti Oinaala on 5.5.2021.
//

import UIKit

final class OwnerViewController: UIViewController {
    var onTaskOwnerSelected: OnTaskOwnerSelected?
    
    private lazy var setupHeaderView: SetupHeaderView = {
        let view = SetupHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Color.contentBackground
        self.view.addSubview(view)
        view.closeButton.addTarget(self, action: #selector(onClosingView(_:)), for: UIControl.Event.touchUpInside)
        return view
    }()
    
    private lazy var tableView: UITableView = {
        var tableView = CustomTableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        tableView.register(OwnerCell.self, forCellReuseIdentifier: String(describing: OwnerCell.self))
        tableView.rowHeight = Setup.tableViewRowHeight
        return tableView
    }()
    private var didLayoutSubviews = false
    private var dataSource: OwnerDataSource?
    
    var owner: String? {
        didSet {
            dataSource = OwnerDataSource()
            dataSource?.owner = owner
            dataSource?.onTaskOwnerSelected = onTaskOwnerSelected
            tableView.reloadData()
        }
    }
    
    override func loadView() {
        super.loadView()
        
        initialize()
    }
}

private extension OwnerViewController {
    func initialize() {
        self.view.backgroundColor = .black
        
        self.tableView.delegate = dataSource
        self.tableView.dataSource = dataSource
        
        NSLayoutConstraint.activate([
            self.setupHeaderView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            self.setupHeaderView.leftAnchor.constraint(equalTo: view.leftAnchor),
            self.setupHeaderView.rightAnchor.constraint(equalTo: view.rightAnchor),
            self.setupHeaderView.heightAnchor.constraint(equalToConstant: 60),
            
            self.tableView.topAnchor.constraint(equalTo: setupHeaderView.bottomAnchor, constant: 10),
            self.tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            self.tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            self.tableView.bottomAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}

// MARK: - Button actions

@objc extension OwnerViewController {
    func onClosingView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
