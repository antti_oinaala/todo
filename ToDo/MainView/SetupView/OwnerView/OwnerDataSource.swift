//
//  OwnerDataSource.swift
//  ToDo
//
//  Created by Antti Oinaala on 5.5.2021.
//

import UIKit
import FontAwesome_swift

final class OwnerDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    private var models = [OwnerViewViewModel]()
    var onTaskOwnerSelected: OnTaskOwnerSelected?
    var owner: String? {
        didSet {
            let model = OwnerViewViewModel(owner: owner)
            models.append(model)
        }
    }
}

// MARK: - Table view delegates

extension OwnerDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = models[indexPath.row]
        let cell = model.representationAsCellOnTableView(tableView: tableView, indexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = models[indexPath.row]
        model.selectOwner()
        model.onTaskOwnerSelected = onTaskOwnerSelected
        tableView.reloadData()
    }
}
