//
//  OwnerViewModel.swift
//  ToDo
//
//  Created by Antti Oinaala on 5.5.2021.
//

import UIKit
import GoogleSignIn

final class OwnerViewViewModel: NSObject {
    private var owner: String?
    private var selected: Bool = false
    var onTaskOwnerSelected: OnTaskOwnerSelected?
    
    convenience init(owner: String?) {
        self.init()
        
        self.owner = owner
        
        if owner != nil {
            selected = true
        }
    }
    
    func representationAsCellOnTableView(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: OwnerCell.self), for: indexPath) as? OwnerCell else { return UITableViewCell() }
        
        setupOwnerCell(cell: cell)
        
        return cell
    }
    
    func selectOwner() {
        selected = selected ? false : true
    }
}

private extension OwnerViewViewModel {
    func setupOwnerCell(cell: OwnerCell) {
        cell.selectionStyle = .none
        cell.backgroundColor = .black
        
        if let owner = owner {
            cell.letterAvatarLabel.name = GIDSignIn.sharedInstance()?.currentUser?.profile.name
            cell.ownerLabel.text = owner
        } else {
            cell.letterAvatarLabel.name = GIDSignIn.sharedInstance()?.currentUser?.profile.name
            cell.ownerLabel.text = GIDSignIn.sharedInstance()?.currentUser?.profile.name
        }
        
        if selected {
            guard let owner = cell.ownerLabel.text else { return }
            onTaskOwnerSelected?(owner)
        } else {
            onTaskOwnerSelected?(nil)
            owner = nil
        }
        
        cell.selectedImageView.isHidden = selected ? false : true
    }
}
