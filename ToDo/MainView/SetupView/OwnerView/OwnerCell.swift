//
//  OwnerCell.swift
//  ToDo
//
//  Created by Antti Oinaala on 5.5.2021.
//

import UIKit

final class OwnerCell: UITableViewCell {
    lazy var contentBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Color.contentBackground
        self.contentView.addSubview(view)
        return view
    }()
    lazy var letterAvatarLabel: LetterAvatarLabel = {
        let label = LetterAvatarLabel()
        label.layer.cornerRadius = LetterAvatar.sizeMid / 2.0
        label.clipsToBounds = true
        label.font = label.font.withSize(Font.sizeBig)
        label.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(label)
        return label
    }()
    
    lazy var ownerLabel: UILabel = {
        let label = UILabel()
        label.font = label.font.withSize(Font.sizeBig)
        label.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(label)
        label.textColor = .lightGray
        return label
    }()
    
    lazy var selectedImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.fontAwesomeIcon(name: .check, style: .solid, textColor: .white, size: CGSize(width: Icon.sizeMid, height: Icon.sizeMid))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(imageView)
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.initialize()
    }
}

private extension OwnerCell {
    func initialize() {
        NSLayoutConstraint.activate([
            self.contentBackgroundView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            self.contentBackgroundView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            self.contentBackgroundView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            self.contentBackgroundView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            self.letterAvatarLabel.centerYAnchor.constraint(equalTo: contentBackgroundView.centerYAnchor),
            self.letterAvatarLabel.leftAnchor.constraint(equalTo: contentBackgroundView.leftAnchor, constant: 5),
            self.letterAvatarLabel.widthAnchor.constraint(equalToConstant: LetterAvatar.sizeMid),
            self.letterAvatarLabel.heightAnchor.constraint(equalToConstant: LetterAvatar.sizeMid),
            
            self.ownerLabel.centerYAnchor.constraint(equalTo: contentBackgroundView.centerYAnchor),
            self.ownerLabel.leftAnchor.constraint(equalTo: letterAvatarLabel.rightAnchor, constant: 5),
            
            self.selectedImageView.centerYAnchor.constraint(equalTo: contentBackgroundView.centerYAnchor),
            self.selectedImageView.leftAnchor.constraint(equalTo: ownerLabel.rightAnchor, constant: 5),
            self.selectedImageView.rightAnchor.constraint(equalTo: contentBackgroundView.rightAnchor, constant: -5),
            self.selectedImageView.heightAnchor.constraint(equalToConstant: Icon.sizeMid),
            self.selectedImageView.widthAnchor.constraint(equalToConstant: Icon.sizeMid),
        ])
    }
}
