//
//  SetupDeleteCell.swift
//  ToDo
//
//  Created by Antti Oinaala on 14.5.2021.
//

import UIKit

final class SetupDeleteCell: UITableViewCell {
    lazy var contentBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Color.contentBackground
        self.contentView.addSubview(view)
        return view
    }()
    lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage.fontAwesomeIcon(name: .trash, style: .solid, textColor: .white, size: CGSize(width: Icon.sizeMid, height: Icon.sizeMid))
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(imageView)
        return imageView
    }()
    
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = titleLabel.font.withSize(Font.sizeTitle)
        titleLabel.textColor = .red
        titleLabel.textAlignment = .center
        titleLabel.text = "Delete"
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(titleLabel)
        return titleLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.initialize()
    }
}

private extension SetupDeleteCell {
    func initialize() {
        NSLayoutConstraint.activate([
            self.contentBackgroundView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            self.contentBackgroundView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            self.contentBackgroundView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            self.contentBackgroundView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -10),
            
            self.iconImageView.centerYAnchor.constraint(equalTo: contentBackgroundView.centerYAnchor),
            self.iconImageView.leftAnchor.constraint(greaterThanOrEqualTo: contentBackgroundView.leftAnchor, constant: 5),
            self.iconImageView.widthAnchor.constraint(equalToConstant: Icon.sizeMid),
            self.iconImageView.heightAnchor.constraint(equalToConstant: Icon.sizeMid),
            
            self.titleLabel.centerYAnchor.constraint(equalTo: iconImageView.centerYAnchor),
            self.titleLabel.centerXAnchor.constraint(equalTo: contentBackgroundView.centerXAnchor),
            self.titleLabel.leftAnchor.constraint(equalTo: iconImageView.rightAnchor, constant: 5),
            self.titleLabel.rightAnchor.constraint(lessThanOrEqualTo: contentBackgroundView.rightAnchor, constant: -5),
        ])
    }
}
