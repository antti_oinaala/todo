//
//  SetupHeaderCell1.swift
//  ToDo
//
//  Created by Antti Oinaala on 30.4.2021.
//

import UIKit

final class SetupHeaderView: UIView {
    lazy var closeButton: UIButton = {
        let button = UIButton()
        let image = UIImage.fontAwesomeIcon(name: .timesCircle, style: .regular, textColor: .white, size: CGSize(width: Icon.sizeBig, height: Icon.sizeBig))
        button.setImage(image, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(button)
        return button
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        initialize()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension SetupHeaderView {
    func initialize() {
        NSLayoutConstraint.activate([
            self.closeButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -10),
            self.closeButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 5),
            self.closeButton.widthAnchor.constraint(equalToConstant: Icon.sizeBig),
            self.closeButton.heightAnchor.constraint(equalToConstant: Icon.sizeBig),
        ])
    }
}
