//
//  SetupViewDataSource.swift
//  ToDo
//
//  Created by Antti Oinaala on 29.4.2021.
//

import UIKit
import FontAwesome_swift

final class SetupViewDataSource: NSObject, UITableViewDelegate, UITableViewDataSource {
    var reloadTableView: () -> Void = {}
    var onUpdatingTaskTable: OnUpdatingTaskTable?
    
    private var task: Table.Task?
    private var currentTableName: String?
    private var models = [SetupViewViewModel]()
    private var onCreatingNewTask = false
    
    func setData(data: SetupViewDataCollection) {
        self.task = data.task
        self.currentTableName = data.tableName
        
        resetModels()
        reloadTableView()
    }
}

// MARK: - Table view delegates

extension SetupViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return models.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = models[indexPath.row]
        let cell = model.representationAsCellOnTableView(tableView: tableView, indexPath: indexPath)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let vc = TaskStateViewController()
            vc.onTaskStateSelected = { [weak self] currentTableName in
                self?.onUpdatingTaskTable?(self?.task, currentTableName, .changeTable)
                self?.currentTableName = currentTableName
                self?.resetModels()
                self?.reloadTableView()
            }
            vc.currentTableName = currentTableName
            UIApplication.shared.topMostViewController()?.present(vc, animated: true, completion: nil)
            return
        case 1:
            let owner = models[indexPath.row].task?.owner
            let vc = OwnerViewController()
            vc.onTaskOwnerSelected = { [weak self] owner in
                guard let task = self?.newTask(owner: owner) else { return }
                self?.onUpdatingTaskTable?(task, self?.currentTableName, .edit)
                self?.task = task
                self?.resetModels()
                self?.reloadTableView()
            }
            vc.owner = owner
            UIApplication.shared.topMostViewController()?.present(vc, animated: true, completion: nil)
            return
            
        case 3:
            let alert = UIAlertController(
                title: "Confirm Deletion",
                message: "Delete the task immediately",
                preferredStyle: UIAlertController.Style.alert
            )
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.default, handler: { _ in }))
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { [weak self] _ in
                let task = self?.models[indexPath.row].getTask()
                self?.onUpdatingTaskTable?(task, self?.currentTableName, .delete)
                UIApplication.shared.topMostViewController()?.dismiss(animated: true, completion: nil)
            }))
            
            UIApplication.shared.topMostViewController()?.present(alert, animated: true, completion: nil)
            
            return
        default:
            return
        }
    }
}

// MARK: - Private functions

private extension SetupViewDataSource {
    func newTask(dateString: String?) -> Table.Task? {
        guard let task = task, let dateString = dateString else { return nil }
        let newTask = Table.Task(id: task.id, body: task.body, expirationDate: dateString, owner: task.owner)
        
        return newTask
    }
    
    func newTask(owner: String?) -> Table.Task? {
        guard let task = task else { return nil }
        let newTask = Table.Task(id: task.id, body: task.body, expirationDate: task.expirationDate, owner: owner)
        
        return newTask
    }
    
    func resetModels() {
        models.removeAll()
        
        guard let tableName = currentTableName, let task = task else { return }
        
        for _ in 0..<Setup.cellNumber {
            let model = SetupViewViewModel(tableName: tableName, task: task)
            model.onDateTimeSelected = { [weak self] dateString in
                guard let task = self?.newTask(dateString: dateString) else { return }
                self?.onUpdatingTaskTable?(task, tableName, .edit)
                self?.task = task
                self?.resetModels()
                self?.reloadTableView()
            }
            models.append(model)
        }
    }
}
