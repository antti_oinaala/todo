//
//  SetupTitleCell.swift
//  ToDo
//
//  Created by Antti Oinaala on 29.4.2021.
//

import UIKit

final class SetupTitleCell: UITableViewCell {
    lazy var contentBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Color.contentBackground
        self.contentView.addSubview(view)
        return view
    }()
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = titleLabel.font.withSize(Font.sizeTitle)
        titleLabel.textColor = .white
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(titleLabel)
        return titleLabel
    }()
    
    private lazy var contentTitleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = titleLabel.font.withSize(Font.sizeBig)
        titleLabel.text = "Table:"
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = .gray
        contentBackgroundView.addSubview(titleLabel)
        return titleLabel
    }()
    
    lazy var contentLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = titleLabel.font.withSize(Font.sizeBig)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.textColor = .lightGray
        contentBackgroundView.addSubview(titleLabel)
        return titleLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.initialize()
    }
}

private extension SetupTitleCell {
    func initialize() {
        NSLayoutConstraint.activate([
            self.contentBackgroundView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            self.contentBackgroundView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            self.contentBackgroundView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            self.contentBackgroundView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            self.titleLabel.topAnchor.constraint(equalTo: contentBackgroundView.topAnchor),
            self.titleLabel.leftAnchor.constraint(equalTo: contentBackgroundView.leftAnchor, constant: 15),
            self.titleLabel.rightAnchor.constraint(equalTo: contentBackgroundView.rightAnchor, constant: -15),
            
            self.contentTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 5),
            self.contentTitleLabel.bottomAnchor.constraint(equalTo: contentBackgroundView.bottomAnchor, constant: -5),
            self.contentTitleLabel.leftAnchor.constraint(equalTo: titleLabel.leftAnchor),
            
            self.contentLabel.topAnchor.constraint(equalTo: contentTitleLabel.topAnchor),
            self.contentLabel.bottomAnchor.constraint(equalTo: contentTitleLabel.bottomAnchor),
            self.contentLabel.leftAnchor.constraint(equalTo: contentTitleLabel.rightAnchor, constant: 5),
            self.contentLabel.rightAnchor.constraint(lessThanOrEqualTo: contentBackgroundView.rightAnchor, constant: -5),
        ])
    }
}
