//
//  SetupCell.swift
//  ToDo
//
//  Created by Antti Oinaala on 29.4.2021.
//

import UIKit

final class SetupCell: UITableViewCell {
    lazy var contentBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Color.contentBackground
        self.contentView.addSubview(view)
        return view
    }()
    lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(imageView)
        return imageView
    }()
    
    lazy var titleLabel: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = titleLabel.font.withSize(Font.sizeBig)
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(titleLabel)
        titleLabel.textColor = .lightGray
        return titleLabel
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.initialize()
    }
}

private extension SetupCell {
    func initialize() {
        NSLayoutConstraint.activate([
            self.contentBackgroundView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            self.contentBackgroundView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            self.contentBackgroundView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            self.contentBackgroundView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            self.iconImageView.centerYAnchor.constraint(equalTo: contentBackgroundView.centerYAnchor),
            self.iconImageView.leftAnchor.constraint(equalTo: contentBackgroundView.leftAnchor, constant: 5),
            self.iconImageView.widthAnchor.constraint(equalToConstant: Icon.sizeMid),
            self.iconImageView.heightAnchor.constraint(equalToConstant: Icon.sizeMid),
            
            self.titleLabel.centerYAnchor.constraint(equalTo: contentBackgroundView.centerYAnchor),
            self.titleLabel.leftAnchor.constraint(equalTo: iconImageView.rightAnchor, constant: 5),
            self.titleLabel.rightAnchor.constraint(equalTo: contentBackgroundView.rightAnchor, constant: -5),
        ])
    }
}
