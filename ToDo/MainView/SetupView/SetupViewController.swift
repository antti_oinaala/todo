//
//  SetupViewController.swift
//  ToDo
//
//  Created by Antti Oinaala on 29.4.2021.
//

import UIKit

final class SetupViewController: UIViewController {
    var onUpdatingTaskTable: OnUpdatingTaskTable?
    
    private lazy var setupHeaderView: SetupHeaderView = {
        let view = SetupHeaderView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Color.contentBackground
        self.view.addSubview(view)
        view.closeButton.addTarget(self, action: #selector(onClosingView(_:)), for: UIControl.Event.touchUpInside)
        return view
    }()
    
    private lazy var tableView: CustomTableView = {
        var tableView = CustomTableView(frame: .zero, style: .plain)
        tableView.separatorStyle = .none
        tableView.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(tableView)
        self.dataSource?.reloadTableView = tableView.reloadData
        tableView.backgroundColor = .black
        tableView.register(SetupTitleCell.self, forCellReuseIdentifier: String(describing: SetupTitleCell.self))
        tableView.register(SetupCell.self, forCellReuseIdentifier: String(describing: SetupCell.self))
        tableView.register(SetupDateTimeCell.self, forCellReuseIdentifier: String(describing: SetupDateTimeCell.self))
        tableView.register(SetupDeleteCell.self, forCellReuseIdentifier: String(describing: SetupDeleteCell.self))
        tableView.rowHeight = Setup.tableViewRowHeight
        return tableView
    }()
    private var didLayoutSubviews = false
    private var dataSource: SetupViewDataSource?
    
    var data: SetupViewDataCollection? {
        didSet {
            guard let data = self.data else { return }
            dataSource = SetupViewDataSource()
            dataSource?.setData(data: data)
            dataSource?.onUpdatingTaskTable = onUpdatingTaskTable
        }
    }
    
    override func loadView() {
        super.loadView()
        
        initialize()
    }
}

private extension SetupViewController {
    func initialize() {
        self.view.backgroundColor = .black
        
        self.tableView.delegate = dataSource
        self.tableView.dataSource = dataSource
        
        NSLayoutConstraint.activate([
            self.setupHeaderView.topAnchor.constraint(equalTo: view.topAnchor),
            self.setupHeaderView.leftAnchor.constraint(equalTo: view.leftAnchor),
            self.setupHeaderView.rightAnchor.constraint(equalTo: view.rightAnchor),
            self.setupHeaderView.heightAnchor.constraint(equalToConstant: 100),
            
            self.tableView.topAnchor.constraint(equalTo: setupHeaderView.bottomAnchor, constant: 10),
            self.tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            self.tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            self.tableView.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor),
        ])
    }
}

// MARK: - Button actions

@objc extension SetupViewController {
    func onClosingView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
