//
//  SetupDatetimeCell.swift
//  ToDo
//
//  Created by Antti Oinaala on 3.5.2021.
//

import UIKit

final class SetupDateTimeCell: UITableViewCell {
    lazy var contentBackgroundView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = Color.contentBackground
        self.contentView.addSubview(view)
        return view
    }()
    lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(imageView)
        return imageView
    }()
    
    lazy var datePickerTextField: DatePickerTextField = {
        let textField = DatePickerTextField()
        textField.font = textField.font?.withSize(Font.sizeBig)
        textField.translatesAutoresizingMaskIntoConstraints = false
        contentBackgroundView.addSubview(textField)
        textField.textColor = .lightGray
        return textField
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        self.initialize()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        self.initialize()
    }
}

private extension SetupDateTimeCell {
    func initialize() {
        NSLayoutConstraint.activate([
            self.contentBackgroundView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 5),
            self.contentBackgroundView.leftAnchor.constraint(equalTo: contentView.leftAnchor),
            self.contentBackgroundView.rightAnchor.constraint(equalTo: contentView.rightAnchor),
            self.contentBackgroundView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            
            self.iconImageView.centerYAnchor.constraint(equalTo: contentBackgroundView.centerYAnchor),
            self.iconImageView.leftAnchor.constraint(equalTo: contentBackgroundView.leftAnchor, constant: 5),
            self.iconImageView.widthAnchor.constraint(equalToConstant: Icon.sizeMid),
            self.iconImageView.heightAnchor.constraint(equalToConstant: Icon.sizeMid),
            
            self.datePickerTextField.centerYAnchor.constraint(equalTo: contentBackgroundView.centerYAnchor),
            self.datePickerTextField.leftAnchor.constraint(equalTo: iconImageView.rightAnchor, constant: 5),
            self.datePickerTextField.rightAnchor.constraint(equalTo: contentBackgroundView.rightAnchor, constant: -5),
        ])
    }
}
