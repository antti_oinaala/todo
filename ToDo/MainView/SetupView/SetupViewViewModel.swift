//
//  SetupViewModel.swift
//  ToDo
//
//  Created by Antti Oinaala on 3.5.2021.
//

import UIKit

final class SetupViewViewModel: NSObject {
    private var tableName: String?
    var task: Table.Task?
    var onDateTimeSelected: OnDateTimeSelected?
    
    convenience init(tableName: String, task: Table.Task) {
        self.init()
        
        self.tableName = tableName
        self.task = task
    }
    
    func representationAsCellOnTableView(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SetupTitleCell.self), for: indexPath) as? SetupTitleCell else { return UITableViewCell() }
            
            setupTitleCell(cell: cell)
            
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SetupCell.self), for: indexPath) as? SetupCell else { return UITableViewCell() }
            
            setupSetupCell(cell: cell)
            
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SetupDateTimeCell.self), for: indexPath) as? SetupDateTimeCell else { return UITableViewCell() }
            
            setupSetupDateTimeCell(cell: cell)
            
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SetupDeleteCell.self), for: indexPath) as? SetupDeleteCell else { return UITableViewCell() }
            
            setupDeleteCell(cell: cell)
            
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    func getTask() -> Table.Task? {
        return task
    }
}

private extension SetupViewViewModel {
    func setupTitleCell(cell: SetupTitleCell) {
        cell.selectionStyle = .none
        cell.contentView.backgroundColor = .black
        
        cell.titleLabel.text = task?.body
        cell.contentLabel.text = tableName
    }
    
    func setupSetupCell(cell: SetupCell) {
        cell.selectionStyle = .none
        cell.contentView.backgroundColor = .black
        
        cell.iconImageView.image = UIImage.fontAwesomeIcon(name: .user, style: .regular, textColor: .white, size: CGSize(width: Icon.sizeMid, height: Icon.sizeMid))
        
        guard let owner = task?.owner else {
            cell.titleLabel.text = "Owner..."
            return
        }
        cell.titleLabel.text = owner
    }
    
    func setupSetupDateTimeCell(cell: SetupDateTimeCell) {
        cell.selectionStyle = .none
        cell.contentView.backgroundColor = .black
        
        cell.iconImageView.image = UIImage.fontAwesomeIcon(name: .calendar, style: .regular, textColor: .white, size: CGSize(width: Icon.sizeMid, height: Icon.sizeMid))
        
        cell.datePickerTextField.dateSelected = { [weak self] dateString in
            self?.onDateTimeSelected?(dateString)
        }
        guard let datetimeString = task?.expirationDate else {
            cell.datePickerTextField.text = "Expiration date..."
            return
        }
        
        cell.datePickerTextField.longDateString = datetimeString
        cell.datePickerTextField.text = Date.shortDateStringFromString(str: datetimeString)
    }
    
    func setupDeleteCell(cell: SetupDeleteCell) {
        cell.selectionStyle = .none
        cell.contentView.backgroundColor = .black
    }
}
