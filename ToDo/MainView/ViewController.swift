//
//  ViewController.swift
//  ToDo
//
//  Created by Antti Oinaala on 20.4.2021.
//

import UIKit
import Gemini
import GoogleSignIn

final class ViewController: UIViewController {
    private lazy var geminiCollectionView: GeminiCollectionView = {
        let collectionView = GeminiCollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.register(TaskListCell.self, forCellWithReuseIdentifier: String(describing: TaskListCell.self))
        collectionView.register(TaskListNewTaskCell.self, forCellWithReuseIdentifier: String(describing: TaskListNewTaskCell.self))
        self.view.addSubview(collectionView)
        return collectionView
    }()
    private lazy var loadingCoverView: UIAlertController = {
        let loadingCoverView = UIAlertController(title: nil, message: "Please wait...", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.medium
        loadingIndicator.startAnimating()
        loadingCoverView.view.addSubview(loadingIndicator)
        return loadingCoverView
    }()
    private var didLayoutSubviews = false
    private var dataSource = DataSource()
    let biometricAuthenticationViewController = BiometricAuthenticationViewController()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard biometricAuthenticationViewController.state == .loggedout else {
            if GIDSignIn.sharedInstance()?.currentUser == nil {
                resetUserAndData()
            }
            
            return
        }
        
        biometricAuthenticationViewController.onBiometricAuthenticationSuccess = { [weak self] in
            if GIDSignIn.sharedInstance()?.currentUser == nil {
                self?.resetUserAndData()
            }
        }
        biometricAuthenticationViewController.modalPresentationStyle = .fullScreen
        present(biometricAuthenticationViewController, animated: false)
    }

    override func loadView() {
        super.loadView()
        
        dataSource.reloadCollectionView = self.geminiCollectionView.reloadData
        
        initialize()
    }
    
    override func viewDidLayoutSubviews() {
        defer {
            didLayoutSubviews = true
            super.viewDidLayoutSubviews()
        }
        
        guard didLayoutSubviews == false else { return }
        
        // Setting of UICollectionViewFlowLayout
        let layout = CollectionViewPagingFlowLayout()
        layout.scrollDirection = .horizontal
        
        layout.itemSize = CGSize(width:geminiCollectionView.frame.width - view.safeAreaInsets.left - view.safeAreaInsets.right - 60, height: geminiCollectionView.frame.height - view.safeAreaInsets.top - view.safeAreaInsets.bottom - 100)
        
        layout.sectionInset = UIEdgeInsets(top: geminiCollectionView.bounds.height/2 - layout.itemSize.height/2 - 80, left: geminiCollectionView.bounds.width/2 - layout.itemSize.width/2, bottom: geminiCollectionView.bounds.height/2 - layout.itemSize.height/2 + 80, right: geminiCollectionView.bounds.width/2 - layout.itemSize.width/2)
        layout.minimumLineSpacing = 20.0
        layout.minimumInteritemSpacing = layout.itemSize.width/2
        geminiCollectionView.collectionViewLayout = layout
        geminiCollectionView.decelerationRate = UIScrollView.DecelerationRate.fast
    }
}

// MARK: - Private functions

private extension ViewController {
    func initialize() {
        self.resetLogButton()
        
        self.geminiCollectionView.backgroundColor = .white
        
        self.geminiCollectionView.dataSource = dataSource
        self.geminiCollectionView.delegate = dataSource
        
        self.geminiCollectionView.allowsSelection = false
        
        NSLayoutConstraint.activate([
            self.geminiCollectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.geminiCollectionView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            self.geminiCollectionView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            self.geminiCollectionView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor)
        ])
    }
    
    func resetLogButton() {
        let button = LogButton(frame: CGRect(x: 0, y: 0, width: 115, height: 25))
        let tap = UITapGestureRecognizer(target: self, action: #selector(onLogAction(_:)))
        button.addGestureRecognizer(tap)
        button.isUserInteractionEnabled = true
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    func resetUserAndData() {
        let vc = AuthenticationViewController()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true) {
            vc.tryToSignInSilently()
        }
        
        vc.onAuthenticationCompleted = { [weak self] givenName in
            self?.resetLogButton()
            
            guard let loadingCoverView = self?.loadingCoverView else { return }
            self?.present(loadingCoverView, animated: false, completion: nil)
            
            self?.dataSource.fetchData().done({ dataInDatabase in
                if !dataInDatabase {
                    self?.dataSource.createDataStructure()
                }
                self?.loadingCoverView.dismiss(animated: true, completion: nil)
            }).catch({ error in
                print(error.localizedDescription)
            })
        }
    }
}

// MARK: - Button actions

@objc extension ViewController {
    func onLogAction(_ sender: Any) {
        if GIDSignIn.sharedInstance()?.currentUser == nil {
            resetUserAndData()
        } else {
            GIDSignIn.sharedInstance()?.signOut()
            dataSource.clearData()
        }
        resetLogButton()
    }
}
