//
//  Task.swift
//  ToDo
//
//  Created by Antti Oinaala on 21.4.2021.
//

import Foundation

struct Table: Codable {
    let id: String
    let name: String
    var tasks: [Task]
    
    struct Task: Codable {
        let id: String
        let body: String
        let expirationDate: String?
        let owner: String?
    }
    
    mutating func appendTask(task: Task) {
        tasks.append(task)
    }
}
