//
//  Calendara+Days.swift
//  ToDo
//
//  Created by Antti Oinaala on 13.5.2021.
//

import Foundation

extension Calendar {
    static func dayCountFromDates(start: Date, end: Date?) -> Int? {
        guard let endDate = end else { return nil }
        guard let date = dayAddByOneToDate(date: endDate) else { return nil }
        let components = current.dateComponents([.day], from: start, to: date)
        
        return components.day
    }
    
    static func dayAddByOneToDate(date: Date) -> Date? {
        return current.date(byAdding: .day, value: 1, to: date)
    }
}
