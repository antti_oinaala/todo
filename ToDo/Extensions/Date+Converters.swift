//
//  Date+Converters.swift
//  ToDo
//
//  Created by Antti Oinaala on 13.5.2021.
//

import Foundation

extension Date {
    static func dateStringFromDate(datetime: Any) -> String? {
        guard let date = datetime as? Date else { return nil }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    
    static func shortDateStringFromString(str: String?) -> String? {
        guard let str = str else { return nil }
        guard let date = dateTimeFromString(str: str) else { return nil }

        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = .none
        dateFormatter.dateFormat = "dd/MM/yyyy"

        let dateString = dateFormatter.string(from: date)
        
        return dateString
    }
    
    static func dateTimeFromString(str: String?) -> Date? {
        guard let str = str else { return nil }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        
        return dateFormatter.date(from: str)
    }
}
