//
//  UIApplication+TopMostViewController.swift
//  ToDo
//
//  Created by Antti Oinaala on 29.4.2021.
//

import UIKit

extension UIApplication {
    func topMostViewController() -> UIViewController? {
        return self.keyWindow?.rootViewController?.topMostViewController()
    }

    var keyWindow: UIWindow? {
        UIApplication.shared.windows.first { $0.isKeyWindow }
    }
}
