//
//  UIViewController+TopViewController.swift
//  ToDo
//
//  Created by Antti Oinaala on 29.4.2021.
//

import UIKit

extension UIViewController {
    func topMostViewController() -> UIViewController {
        if self.presentedViewController == nil {
            return self
        }
        if let navigation = self.presentedViewController as? UINavigationController {
            guard let vc = navigation.visibleViewController?.topMostViewController() else {
                return UIViewController()
            }
            return vc
        }
        
        guard let vc = presentedViewController?.topMostViewController() else {
            return UIViewController()
        }
        return vc
    }
}
