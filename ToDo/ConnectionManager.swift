//
//  ConnectionManager.swift
//  ToDo
//
//  Created by Antti Oinaala on 6.4.2021.
//

import Foundation
import PromiseKit

struct ConnectionManager {
    static let shared = ConnectionManager()
    
    private let session = URLSession.shared
    
    func fetchData() -> Promise<[Table]> {
        return Promise { seal in
            var urlComponents = URLComponents()
            urlComponents.scheme = "https"
            urlComponents.host = URL.baseURLPath
            urlComponents.path = "/api/tables"
            
            guard let url = urlComponents.url else {
                return seal.resolve(NSError(domain: "0", code: -1, userInfo: ["Error": "Failed to compose URL with components."]), nil)
            }
            
            var request = URLRequest(url: url)
            request.httpMethod = "GET"
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            session.dataTask(with: request) { (data, response, error) in
                if let error = error {
                    return seal.resolve(error, nil)
                }
                
                guard let data = data else {
                    return seal.resolve(NSError(domain: "0", code: 404, userInfo: ["Error": "Reading: Bad request."]), nil)
                }
                
                var decodingErrorString = ""
                
                do {
                    let data = try JSONDecoder().decode([Table].self, from: data)
                    return seal.resolve(nil, data)
                } catch let DecodingError.dataCorrupted(context) {
                    decodingErrorString = context.debugDescription
                } catch let DecodingError.keyNotFound(key, context) {
                    decodingErrorString = "Key '\(key)' not found: \(context.debugDescription) \n codingPath: \(context.codingPath)"
                } catch let DecodingError.valueNotFound(value, context) {
                    decodingErrorString = "Key '\(value)' not found: \(context.debugDescription) \n codingPath: \(context.codingPath)"
                } catch let DecodingError.typeMismatch(type, context)  {
                    decodingErrorString = "Key '\(type)' not found: \(context.debugDescription) \n codingPath: \(context.codingPath)"
                } catch {
                    print("error: ", error)
                }
                return seal.resolve(NSError(domain: "0", code: 404, userInfo: ["Error": "Updating: \(decodingErrorString)"]), nil)
            }.resume()
        }
    }
    
    func saveData(data: Table) -> Promise<Table> {
        return Promise { seal in
            var urlComponents = URLComponents()
            urlComponents.scheme = "https"
            urlComponents.host = URL.baseURLPath
            urlComponents.path = "/api/tables"
            
            guard let url = urlComponents.url else {
                return seal.resolve(NSError(domain: "0", code: -1, userInfo: ["Error": "Failed to compose URL with components."]), nil)
            }
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            do {
                let dictionary = try? data.asDictionary()
                
                guard JSONSerialization.isValidJSONObject(dictionary as Any) == true else {
                    return seal.resolve(NSError(domain: "0", code: -1, userInfo: ["Error": "Writing: Invalid json conversion."]), nil)
                }
                
                let httpBody = try? JSONSerialization.data(withJSONObject: dictionary as Any, options: [.prettyPrinted])
                request.httpBody = httpBody
                request.timeoutInterval = 20.0
                
                session.dataTask(with: request) { (data, response, error) in
                    if let error = error {
                        return seal.resolve(error, nil)
                    }
                    
                    guard let data = data else {
                        return seal.resolve(NSError(domain: "0", code: 404, userInfo: ["Error": "Reading: Bad request."]), nil)
                    }
                    
                    var decodingErrorString = ""
                    
                    do {
                        let data = try JSONDecoder().decode(Table.self, from: data)
                        return seal.resolve(nil, data)
                    } catch let DecodingError.dataCorrupted(context) {
                        decodingErrorString = context.debugDescription
                    } catch let DecodingError.keyNotFound(key, context) {
                        decodingErrorString = "Key '\(key)' not found: \(context.debugDescription) \n codingPath: \(context.codingPath)"
                    } catch let DecodingError.valueNotFound(value, context) {
                        decodingErrorString = "Key '\(value)' not found: \(context.debugDescription) \n codingPath: \(context.codingPath)"
                    } catch let DecodingError.typeMismatch(type, context)  {
                        decodingErrorString = "Key '\(type)' not found: \(context.debugDescription) \n codingPath: \(context.codingPath)"
                    } catch {
                        print("error: ", error)
                    }
                    return seal.resolve(NSError(domain: "0", code: 404, userInfo: ["Error": "Updating: \(decodingErrorString)"]), nil)
                }.resume()
            }
        }
    }
    
    func updateData(data: Table) -> Promise<Table> {
        return Promise { seal in
            var urlComponents = URLComponents()
            urlComponents.scheme = "https"
            urlComponents.host = URL.baseURLPath
            urlComponents.path = "/api/tables/\(data.id)"
            
            guard let url = urlComponents.url else {
                return seal.resolve(NSError(domain: "0", code: -1, userInfo: ["Error": "Failed to compose URL with components."]), nil)
            }
            
            var request = URLRequest(url: url)
            request.httpMethod = "PUT"
            request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
            
            do {
                let dictionary = try? data.asDictionary()
                
                guard JSONSerialization.isValidJSONObject(dictionary as Any) == true else {
                    return seal.resolve(NSError(domain: "0", code: -1, userInfo: ["Error": "Writing: Invalid json conversion."]), nil)
                }
                
                let httpBody = try? JSONSerialization.data(withJSONObject: dictionary as Any, options: [.prettyPrinted])
                request.httpBody = httpBody
                request.timeoutInterval = 20.0
                
                session.dataTask(with: request) { (data, response, error) in
                    if let error = error {
                        print(error.localizedDescription)
                    }
                    
                    guard let data = data else {
                        return seal.resolve(NSError(domain: "0", code: 404, userInfo: ["Error": "Reading: Bad request."]), nil)
                    }
                    
                    var decodingErrorString = ""
                    
                    do {
                        let data = try JSONDecoder().decode(Table.self, from: data)
                        return seal.resolve(nil, data)
                    } catch let DecodingError.dataCorrupted(context) {
                        decodingErrorString = context.debugDescription
                    } catch let DecodingError.keyNotFound(key, context) {
                        decodingErrorString = "Key '\(key)' not found: \(context.debugDescription) \n codingPath: \(context.codingPath)"
                    } catch let DecodingError.valueNotFound(value, context) {
                        decodingErrorString = "Key '\(value)' not found: \(context.debugDescription) \n codingPath: \(context.codingPath)"
                    } catch let DecodingError.typeMismatch(type, context)  {
                        decodingErrorString = "Key '\(type)' not found: \(context.debugDescription) \n codingPath: \(context.codingPath)"
                    } catch {
                        print("error: ", error)
                    }
                    return seal.resolve(NSError(domain: "0", code: 404, userInfo: ["Error": "Updating: \(decodingErrorString)"]), nil)
                }.resume()
            }
        }
    }
}
